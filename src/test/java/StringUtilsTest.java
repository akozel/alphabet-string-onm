import by.akozel.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTest {

    @Test
    public void express_test() {
        String string = "Azcba1";
        String alphabet = "123AaBbCcZz";

        String result = StringUtils.orderWithDuplicates(alphabet, string);
        System.out.println("result is: " + result);

        Assert.assertEquals("1Aabcz", result);
    }

    @Test
    public void repetitions_test() {
        String string = "Azcbaa1";
        String alphabet = "123AaBbCcZz";

        String result = StringUtils.orderWithDuplicates(alphabet, string);
        System.out.println("result is: " + result);

        Assert.assertEquals("1Aaabcz", result);
    }

    @Test
    public void char_isNotPresent_in_alphabet_test() {
        String string = "!Azcba1";
        String alphabet = "123AaBbCcZz";

        String result = StringUtils.orderWithDuplicates(alphabet, string);
        System.out.println("result is: " + result);
        System.out.println(string);

        Assert.assertEquals("1Aabcz", result);
    }

}
