package by.akozel;

import java.util.HashMap;
import java.util.Map;

public class StringUtils {

    public static String orderWithDuplicates(String alphabet, String string) {

        Map<String, String> map = new HashMap<>();

        for (int i = 0; i < string.length(); i++) {
            String character = String.valueOf(string.charAt(i));
            map.putIfAbsent(character, "");
            map.computeIfPresent(character, (key, value) -> value + key);
        }

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < alphabet.length(); i++) {
            String character = String.valueOf(alphabet.charAt(i));
            String replaceWith = map.getOrDefault(character, "");

            result.append(replaceWith);
        }

        return result.toString();
    }

}
